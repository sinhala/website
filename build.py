""" ස්ථිතික පිටු සකසන ප්‍රධාන පිටපත """

import os
from shutil import copytree, rmtree
from htmlmin.minify import html_minify
from flask import render_template
from utils import remove_folder, make_folder_if_not_exist, cleanup_or_make_folder, app
from data import PATH_TMP, PATH_PUBLIC, PATH_STATIC, PATH_PUBLIC_STATIC, PATH_PUBLIC_PRIVACY
import words
import akuru
import input_help as inputHelp

def copy_static():
    """ ස්ථිතික ගොනු පිටපත් කරන්න """
    if os.path.exists(PATH_PUBLIC_STATIC):
        rmtree(PATH_PUBLIC_STATIC)
    else:
        make_folder_if_not_exist(PATH_PUBLIC)
    copytree(PATH_STATIC, PATH_PUBLIC_STATIC)

def build_common_pages():
    """ වෙබ් අඩවියේ සියලු ස්ථානවලට පොදු පිටු සැකසීම """
    with open(os.path.join(PATH_PUBLIC, "index.html"), "w", encoding="utf-8") as write_file:
        with app.test_request_context("/"):
            write_file.write(html_minify(render_template('home.html')))

    cleanup_or_make_folder(PATH_PUBLIC_PRIVACY)
    with open(os.path.join(PATH_PUBLIC_PRIVACY, "index.html"), "w", encoding="utf-8") as write_file:
        with app.test_request_context("/privacy/"):
            write_file.write(html_minify(render_template('privacy.html')))

print("Available options:")
print("1 = Cleanup")
print("2 = Create All")
print("3 = Create Words")
print("4 = Create Akuru")
print("5 = Create Input Info")
choice = int(input("Enter an option number: "))
print("")
if choice in [1,2,3,4,5]:
    if choice == 1:
        print("Cleanup", end="\r", flush=True)
        remove_folder(PATH_TMP)
        remove_folder(PATH_PUBLIC)
        print("Cleanup: Done")
    else:
        cleanup_or_make_folder(PATH_TMP)
        make_folder_if_not_exist(PATH_PUBLIC)
        copy_static()
        build_common_pages()

    if choice == 2:
        print("Creating all")
        words.build()
        akuru.build()
        inputHelp.build()
    elif choice == 3:
        words.build()
    elif choice == 4:
        akuru.build()
    elif choice == 5:
        inputHelp.build()
