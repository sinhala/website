""" විවිධ උපකාරක කොටස් """

import os
from shutil import rmtree
import subprocess
from flask import Flask

app = Flask(__name__)
@app.route('/')
def page_home(): pass # pylint: disable=C0116,C0321
@app.route('/info/')
def page_info(): pass  # pylint: disable=C0116,C0321
@app.route('/privacy/')
def page_privacy(): pass # pylint: disable=C0116,C0321
@app.route('/words/')
def page_words(): pass # pylint: disable=C0116,C0321
@app.route('/words/list/<int:id>/')
def page_words_list(): pass # pylint: disable=C0116,C0321
@app.route('/akuru/')
def page_akuru(): pass # pylint: disable=C0116,C0321
@app.route('/akuru/<int:id>/')
def page_akuru_info(id): pass # pylint: disable=C0116,C0321
@app.route('/input/')
def page_input_help(): pass # pylint: disable=C0116,C0321
@app.route('/input/<string:distro>/<string:version>')
def page_input_help_distro_version(): pass # pylint: disable=C0116,C0321
@app.route('/input/report')
def page_input_report(): pass # pylint: disable=C0116,C0321

def clone_or_pull_repo(repo_path, repo_url):
    """ git ගබඩාවක් පිටපත් කිරීම හෝ පවතියි නම් යාවත්කාල කිරීම """
    if os.path.isdir(repo_path):
        subprocess.run(["git", "-C", repo_path, "pull"], check=True)
    else:
        subprocess.run(["git", "clone", repo_url, repo_path], check=True)

def make_folder_if_not_exist(folder_path):
    """ නාමාවලියක් නැති නම් පමණක් නව නාමාවලියක් සැකසීම """
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)

def cleanup_or_make_folder(folder_path):
    """ නව නාමාවලියක් සැකසීම හෝ පවතින නාමාවලියක් හිස් කිරීම """
    if os.path.exists(folder_path):
        for root, dirs, files in os.walk(folder_path):
            for file_path in files:
                os.unlink(os.path.join(root, file_path))
            for dir_path in dirs:
                rmtree(os.path.join(root, dir_path))
    else:
        os.makedirs(folder_path)

def remove_folder(folder_path):
    """ නාමාවලියක් මකා දැමීම """
    if os.path.exists(folder_path):
        rmtree(folder_path)

def get_file_size(file_path):
    """ ගොනුවක ප්‍රමාණය, කියවීමට පහසු ලෙස ලබාදීම """
    file_size = os.path.getsize(file_path)
    step = 1024
    size_names = {"GB": 3, "MB": 2, "KB": 1}
    for name, i in size_names.items():
        if file_size >= step ** i:
            return f'{round(file_size / step * i, 1)}{name}'
    return f'{file_size}B'

def get_page_nav(page_count, page_number):
    """ පෙන්විය යුතු පිටු අංක ලබාදීම """
    nav_tmp = []
    for i in range(1, page_count + 1):
        if i == 1:
            nav_tmp.append(i)
        elif i == page_number - 200:
            nav_tmp.append(i)
        elif i == page_number - 100:
            nav_tmp.append(i)
        elif i == page_number - 50:
            nav_tmp.append(i)
        elif i == page_number - 20:
            nav_tmp.append(i)
        elif i == page_number - 10:
            nav_tmp.append(i)
        elif page_number - 3 < i < page_number + 3:
            nav_tmp.append(i)
        elif i == page_number + 10:
            nav_tmp.append(i)
        elif i == page_number + 20:
            nav_tmp.append(i)
        elif i == page_number + 50:
            nav_tmp.append(i)
        elif i == page_number + 100:
            nav_tmp.append(i)
        elif i == page_number + 200:
            nav_tmp.append(i)
        elif i == page_count:
            nav_tmp.append(i)
    nav = []

    for i, value in enumerate(nav_tmp):
        nav.append(value)
        if i + 1 < len(nav_tmp) and value < nav_tmp[i + 1] - 1:
            nav.append(0)
    return nav
