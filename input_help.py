""" 'යතුරු ලිවීම' පිටු සකසන පිටපත """

# pylint: disable=line-too-long

from os import path, listdir
import tomllib
from htmlmin.minify import html_minify
from flask import render_template
from utils import clone_or_pull_repo, cleanup_or_make_folder, app
from data import PATH_PUBLIC, WORKING_DIR_PATH

PATH_PUBLIC_INPUT = path.join(PATH_PUBLIC, "input")
PATH_REPO = path.join(WORKING_DIR_PATH, ".repo_input_help")
URL_REPO = "https://codeberg.org/sinhala/input_help.git"
PATH_DISTRO_DATA = path.join(PATH_REPO, "data")
distros = {}

def load_distro_data():
    """ මෙහෙයුම් පද්ධති පිළිබඳ තොරතුරු සූදානම් කිරීම """
    for distro_code in sorted(listdir(PATH_DISTRO_DATA)):
        if not path.isdir(path.join(PATH_DISTRO_DATA, distro_code)):
            continue
        path_distro = path.join(PATH_DISTRO_DATA, distro_code)
        with open(path.join(path_distro, "info.toml"), mode="rb") as read_file:
            distros[distro_code] = tomllib.load(read_file)
        distros[distro_code]["versions"] = {}
        for version_file_name in listdir(path_distro):
            if version_file_name == "info.toml":
                continue
            with open(path.join(path_distro, version_file_name), mode="rb") as read_file:
                distros[distro_code]["versions"][version_file_name.replace(".toml", "")] = tomllib.load(read_file)

def create_index_page():
    """ 'යතුරු ලිවීම' පිටුවල මුල පිටුව සැකසීම """
    with open(path.join(PATH_PUBLIC_INPUT, "index.html"), "w", encoding="utf-8") as write_file:
        with app.test_request_context("/input/"):
            write_file.write(html_minify(render_template("input/index.html", distros=distros)))

def create_version_page(distro_code, distro_info, version_code, version_info):
    """ මෙහෙයුම් පද්ධතියක සංස්කරණයකට අදාළ පිටුව සැකසීම """
    path_version = path.join(PATH_PUBLIC_INPUT, distro_code, version_code)
    cleanup_or_make_folder(path_version)
    with app.test_request_context(f'/input/{distro_code}/{version_code}'):
        with open(path.join(path_version, "index.html"), "w", encoding="utf-8") as write_file:
            write_file.write(html_minify(render_template("input/version/index.html", distroInfo=distro_info, versionInfo=version_info)))

def create_report_page():
    """ දෝෂ වාර්තා කිරීම පිටුව සැකසීම """
    softwares = {}
    with open(path.join(PATH_DISTRO_DATA, "report.toml"), mode="rb") as read_file:
        softwares = tomllib.load(read_file)["softwares"]
    path_report = path.join(PATH_PUBLIC_INPUT, "report")
    cleanup_or_make_folder(path_report)
    with open(path.join(path_report, "index.html"), "w", encoding="utf-8") as write_file:
        with app.test_request_context("/input/report"):
            write_file.write(html_minify(render_template("input/report/index.html", softwares=softwares)))

def create_version_pages():
    """ මෙහෙයුම් පද්ධතිවල සංස්කරණවලට අදාළ පිටු සැකසීම """
    for distro_code, distro_info in distros.items():
        for version_code, version_info in distro_info["versions"].items():
            create_version_page(distro_code, distro_info, version_code, version_info)

def build():
    """ 'යතුරු ලිවීම' පිටු සැකසීම """
    cleanup_or_make_folder(PATH_PUBLIC_INPUT)
    clone_or_pull_repo(PATH_REPO, URL_REPO)
    load_distro_data()
    create_index_page()
    create_version_pages()
    create_report_page()
