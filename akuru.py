""" අකුරු පිටු සකසන පිටපත """

# pylint: disable=line-too-long

import re
import os
import subprocess
import multiprocessing
from shutil import copyfile, rmtree
from cairosvg import svg2png
from svgpathtools import svg2paths, parse_path
from htmlmin.minify import html_minify
from flask import render_template
from utils import clone_or_pull_repo, cleanup_or_make_folder, app, get_file_size, make_folder_if_not_exist
from data import WORKING_DIR_PATH, PATH_PUBLIC, PATH_PUBLIC_STATIC, PATH_TEMPLATES, akuru as data_akuru, PATH_STATIC
from data import PATH_TMP, swara_keti as data_swara_keti, swara_deerga as data_swara_deerga, alpaprana as data_alpaprana
from data import mahaprana as data_mahaprana

PATH_PUBLIC_AKURU = os.path.join(PATH_PUBLIC, "akuru")
PATH_TEMPLATES_SVG = os.path.join(PATH_TEMPLATES, ".akuru", "svg")
PATH_REPO = os.path.join(WORKING_DIR_PATH, ".repo_akuru")
URL_REPO = "https://codeberg.org/sinhala/akuru.git"

DOWNLOADS_THEME_LIGHT = 1
DOWNLOADS_THEME_NIGHT = 2
DOWNLOADS_THEME_COLOR = 3

VIDEO_SIZE = 1080
FRAME_RATE = 30
FRAME_COUNT_MULTIPLIER = 2

RE_PATTERN_SVG_VIEWBOX = r'viewBox=\"0 0 ([0-9]+\.?[0-9]*) 100\"'
RE_PATTERN_SVG_MARKER_START = r'm([0-9]+\.?[0-9]*)'
RE_PATTERN_SVG_FILL = r'\s?fill=\"[a-z]+\"'
RE_PATTERN_SVG_STROKE = r'\s?stroke=\"#[0-9]+\"'
RE_PATTERN_SVG_STROKE_LINECAP = r'\s?stroke-linecap=\"[a-z]+\"'
RE_PATTERN_SVG_STROKE_LINEJOIN = r'\s?stroke-linejoin=\"[a-z]+\"'

def create_index_page():
    """ අකුරු පිටුවල මුල පිටුව ගොනුව සැකසීම """
    with open(os.path.join(PATH_PUBLIC_AKURU, "index.html"), "w", encoding="utf-8") as write_file:
        with app.test_request_context("/akuru/"):
            write_file.write(html_minify(render_template('akuru/index.html')))

def create_svgs():
    """ SVG ගොනු සැකසීම """
    cleanup_or_make_folder(PATH_TEMPLATES_SVG)
    if os.path.exists(PATH_REPO):
        file_name_number_regex = re.compile(r'(^[0-9]+)')
        for root, dirs, files in os.walk(os.path.join(PATH_REPO, "අකුරු")): # pylint: disable=unused-variable
            for svg_file in files:
                if svg_file.endswith("min.svg"):
                    svg_file_path = os.path.join(root, svg_file)
                    with open(svg_file_path, "r", encoding="utf-8") as read_file:
                        svg_content = read_file.read()
                        viewbox_width = re.search(RE_PATTERN_SVG_VIEWBOX, svg_content).group(1)
                        marker_shift = ((100 - (float(viewbox_width) - 1)) / 2) - 0.5
                        markers = re.findall(RE_PATTERN_SVG_MARKER_START, svg_content)
                        for marker in markers:
                            new_marker = round(float(marker) + marker_shift, 4)
                            svg_content = svg_content.replace(f'm{marker}', f'm{new_marker}')
                        svg_content = svg_content.replace(f'viewBox=\"0 0 {viewbox_width} 100\"', "viewBox=\"0 0 100 100\"")
                        svg_content = re.sub(RE_PATTERN_SVG_FILL, "", svg_content)
                        svg_content = re.sub(RE_PATTERN_SVG_STROKE, "", svg_content)
                        svg_content = re.sub(RE_PATTERN_SVG_STROKE_LINECAP, "", svg_content)
                        svg_content = re.sub(RE_PATTERN_SVG_STROKE_LINEJOIN, "", svg_content)
                        dst_file_name = str(int(file_name_number_regex.search(svg_file).group(1))) + ".svg"
                        with open(os.path.join(PATH_TEMPLATES_SVG, dst_file_name), "w", encoding="utf-8") as write_file:
                            write_file.write(svg_content)

def create_play_css():
    """ අකුරු ධාවනය කිරීමට අවශ්‍ය CSS ගොනුව සැකසීම """
    lengths = {}
    for letter_id in data_akuru:
        svg_file = os.path.join(PATH_TEMPLATES_SVG, f'{letter_id}.svg')
        lengths[letter_id] = [round(svg_path.length() + 0.005, 2) for svg_path in svg2paths(svg_file)[0]]
    with app.test_request_context():
        make_folder_if_not_exist(PATH_PUBLIC_STATIC)
        with open(os.path.join(PATH_PUBLIC_STATIC, "css", "play.css"), "w", encoding="utf-8") as write_file:
            write_file.write(render_template('akuru/id/play.css', lengths=lengths))

def create_rules_png(colored):
    """ වර්ණ තේමාවට අදාළව පසුබිම් පින්තූරය සැකසීම """
    name = "rules_colored" if colored else "rules"
    with open(os.path.join(PATH_STATIC, f'{name}.svg'), "r", encoding="utf-8") as read_file:
        new_svg_content = read_file.read().replace('viewBox="0 0 100 100"', f'viewBox="0 0 100 100" width="{VIDEO_SIZE}" height="{VIDEO_SIZE}"')
        svg2png(bytestring=new_svg_content.encode('utf-8'), write_to=os.path.join(PATH_TMP, f'{name}.png'))

def create_media_downloads(letter_id, downloads_theme):
    """ අකුරකට සහ වර්ණ තේමාවකට අදාළ බාගැනීම් සැකසීම """
    theme = ""
    bg_color = ""
    fg_color = ""
    rules_image_name = ""

    if downloads_theme == DOWNLOADS_THEME_LIGHT:
        theme = "light"
        bg_color = "d9d9d9"
        fg_color = "262626"
        rules_image_name = "rules.png"
    elif downloads_theme == DOWNLOADS_THEME_NIGHT:
        theme = "night"
        bg_color = "262626"
        fg_color = "d9d9d9"
        rules_image_name = "rules.png"
    elif downloads_theme == DOWNLOADS_THEME_COLOR:
        theme = "color"
        bg_color = "ffee58"
        fg_color = "000000"
        rules_image_name = "rules_colored.png"

    path_tmp_frames = os.path.join(PATH_TMP, f'frames_{letter_id}_{theme}')
    cleanup_or_make_folder(path_tmp_frames)
    src_file = os.path.join(PATH_TEMPLATES_SVG, f'{letter_id}.svg')
    with open(src_file, "r", encoding="utf-8") as read_file:
        svg_content = read_file.read().replace('viewBox="0 0 100 100"', f'viewBox="0 0 100 100" width="{VIDEO_SIZE}" height="{VIDEO_SIZE}"')
        path_data_list = []
        total_length = 0
        for i, svg_path in enumerate(re.findall(r'd="[a-zA-Z0-9\s\.-]+"', svg_content)):
            path_string = re.search(r'"([a-zA-Z0-9\s\.-]+)"', svg_path).group(1)
            path_length = round(parse_path(path_string).length() + 0.005, 2)
            path_data_list.append([svg_path, path_length, total_length])
            total_length += path_length
            new_svg_path = f'stroke-dasharray="_da_{i}_" stroke-dashoffset="_do_{i}_" {svg_path} fill="none" stroke="#{fg_color}" stroke-linecap="round" stroke-linejoin="round"'
            svg_content = svg_content.replace(svg_path, new_svg_path)
        total_frames = round(total_length / 15) * FRAME_RATE * FRAME_COUNT_MULTIPLIER

        ### Create Image(s) ###
        print(f'{"":<12}image ({theme}): started')
        path_tmp_image = os.path.join(PATH_TMP, f'image_{letter_id}_{theme}.png')
        svg2png(bytestring=svg_content.replace(f'_da_{i}_', "").replace(f'_do_{i}_', "").encode('utf-8'), write_to=path_tmp_image)

        ### Without background ###
        if downloads_theme == DOWNLOADS_THEME_LIGHT:
            copyfile(
                path_tmp_image,
                os.path.join(PATH_PUBLIC_AKURU, str(letter_id), f'akuru_image_{str(letter_id).zfill(2)}_nobg.png')
            )

        ### With background ###
        subprocess.run(
            ["convert",
                "-composite", os.path.join(PATH_TMP, rules_image_name), path_tmp_image,
                "-background", f'#{bg_color}',
                "-flatten",
                os.path.join(PATH_PUBLIC_AKURU, str(letter_id), f'akuru_image_{str(letter_id).zfill(2)}_{theme}.png')
            ], check=True
        )
        print(f'{"":<12}image ({theme}): done   ')
        os.unlink(path_tmp_image)

        ### Create Video ###
        print(f'{"":<12}video ({theme}): frames: started')
        for frame in range(total_frames + 1):
            new_svg_content = svg_content
            for i, path_pata in enumerate(path_data_list):
                svg_path, path_length, length_before_start = path_pata
                path_length_current = max((total_length * frame / total_frames) - length_before_start, 0)
                path_dash_offset = max(round(path_length - path_length_current, 4), 0)
                new_svg_content = new_svg_content.replace(f'_da_{i}_', str(path_length)).replace(f'_do_{i}_', str(path_dash_offset))
            svg2png(bytestring=new_svg_content.encode('utf-8'), write_to=os.path.join(path_tmp_frames, f'{str(frame).zfill(5)}.png'))
        print(f'{"":<12}video ({theme}): frames: done   ')
    print(f'{"":<12}video ({theme}): ffmpeg: started')
    subprocess.run(
        ["ffmpeg",
            "-f", "lavfi",
            "-i", f'color={bg_color}:{VIDEO_SIZE}x{VIDEO_SIZE}',
            "-loop", "1",
            "-framerate", "1",
            "-i", os.path.join(PATH_TMP, rules_image_name),
            "-framerate", str(FRAME_RATE * FRAME_COUNT_MULTIPLIER),
            "-thread_queue_size", "1024",
            "-i", f'{path_tmp_frames}/%05d.png',
            "-filter_complex", f'[0][1]overlay=0:0,minterpolate=fps={FRAME_RATE * FRAME_COUNT_MULTIPLIER}:mi_mode=blend,fps=fps={FRAME_RATE}[b];[b][2]overlay=0:0:shortest=1,fps=fps={FRAME_RATE}',
            "-vcodec", "libx264",
            "-preset", "veryslow",
            "-tune", "fastdecode",
            "-crf", "23",
            "-pix_fmt", "yuv420p",
            "-r", str(FRAME_RATE),
            os.path.join(PATH_PUBLIC_AKURU, str(letter_id), f'akuru_video_{str(letter_id).zfill(2)}_{theme}.mp4'),
            "-v", "error"
        ], check=True
    )
    print(f'{"":<12}video ({theme}): ffmpeg: done   ')
    rmtree(path_tmp_frames)

def create_downloads(letter_id):
    """ බාගැනීම් සැකසීම """
    print(f'{"":<8}creating downloads')
    cleanup_or_make_folder(os.path.join(PATH_PUBLIC_AKURU, str(letter_id)))

    proc_light = multiprocessing.Process(target=create_media_downloads, args=(letter_id, DOWNLOADS_THEME_LIGHT))
    proc_night = multiprocessing.Process(target=create_media_downloads, args=(letter_id, DOWNLOADS_THEME_NIGHT))
    proc_color = multiprocessing.Process(target=create_media_downloads, args=(letter_id, DOWNLOADS_THEME_COLOR))

    proc_light.start()
    proc_night.start()
    proc_color.start()

    proc_light.join()
    proc_night.join()
    proc_color.join()

def create_page(letter_id):
    """ අකුරට අදාළ පිටුව සැකසීම """
    print(f'{"":<8}creating page', end="\r", flush=True)
    with app.test_request_context(f'/akuru/{letter_id}/'):
        path_public_akuru_index = os.path.join(PATH_PUBLIC_AKURU, str(letter_id))
        download_sizes = {
            "video_light": get_file_size(os.path.join(path_public_akuru_index, f'akuru_video_{str(letter_id).zfill(2)}_light.mp4')),
            "video_night": get_file_size(os.path.join(path_public_akuru_index, f'akuru_video_{str(letter_id).zfill(2)}_night.mp4')),
            "video_color": get_file_size(os.path.join(path_public_akuru_index, f'akuru_video_{str(letter_id).zfill(2)}_color.mp4')),
            "image_light": get_file_size(os.path.join(path_public_akuru_index, f'akuru_image_{str(letter_id).zfill(2)}_light.png')),
            "image_night": get_file_size(os.path.join(path_public_akuru_index, f'akuru_image_{str(letter_id).zfill(2)}_night.png')),
            "image_color": get_file_size(os.path.join(path_public_akuru_index, f'akuru_image_{str(letter_id).zfill(2)}_color.png')),
            "image_nobg": get_file_size(os.path.join(path_public_akuru_index, f'akuru_image_{str(letter_id).zfill(2)}_nobg.png'))
        }
        with open(os.path.join(path_public_akuru_index, 'index.html'), 'w', encoding="utf-8") as file:
            info = {
                'id': letter_id,
                'letter': data_akuru[letter_id],
                'swara': letter_id <= 18,
                'keti_swara': data_akuru[letter_id] in data_swara_keti,
                'deerga_swara': data_akuru[letter_id] in data_swara_deerga,
                'alpaprana': data_akuru[letter_id] in data_alpaprana,
                'mahaprana': data_akuru[letter_id] in data_mahaprana,
                'download_sizes': download_sizes
            }
            file.write(html_minify(render_template('akuru/id/index.html', id=id, info=info)))
            print(f'{"":<8}creating page: done')

def build():
    """ අකුරු පිටු සැකසීම """
    cleanup_or_make_folder(PATH_PUBLIC_AKURU)
    clone_or_pull_repo(PATH_REPO, URL_REPO)
    create_svgs()
    create_index_page()
    create_play_css()

    create_rules_png(False)
    create_rules_png(True)
    for letter_id in data_akuru:
        print(f'    letter: {letter_id}')
        create_downloads(letter_id)
        create_page(letter_id)
