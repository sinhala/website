""" වචන පිටු සකසන පිටපත """

# pylint: disable=line-too-long

import os
from flask import render_template
from htmlmin.minify import html_minify
from utils import clone_or_pull_repo, cleanup_or_make_folder, get_page_nav, app
from data import PATH_PUBLIC, WORKING_DIR_PATH

path_public_words = os.path.join(PATH_PUBLIC, "words")
path_repo = os.path.join(WORKING_DIR_PATH, ".repo_words")
URL_REPO = "https://codeberg.org/sinhala/words.git"

def create_index_page():
    """ වචන පිටුවල මුල පිටුව ගොනුව සැකසීම """
    with open(os.path.join(path_public_words, "index.html"), "w", encoding="utf-8") as write_file:
        with app.test_request_context("/words/"):
            write_file.write(html_minify(render_template('words/index.html')))

def create_list_pages():
    """ වචන ලැයිස්තු පිටු සැකසීම """
    words = []
    with open(os.path.join(path_repo, "correct_words"), "r", encoding="utf-8") as read_file:
        for line in read_file.readlines():
            words.append(line.strip())
    words.sort()
    words_per_page = 20
    words_page_count = -len(words) // words_per_page * -1
    for page_number in range(1, words_page_count + 1):
        words_start_position = (words_per_page * page_number) - words_per_page
        words_end_position = min(len(words), (words_per_page * page_number))
        cleanup_or_make_folder(os.path.join(path_public_words, "list", str(page_number)))
        nav = get_page_nav(words_page_count, page_number)
        with open(os.path.join(path_public_words, "list", str(page_number), "index.html"), "w", encoding="utf-8") as write_file:
            with app.test_request_context(f'/words/list/{page_number}/'):
                write_file.write(html_minify(render_template('words/list/index.html', words=words[words_start_position:words_end_position], nav=nav, pageNumber=page_number)))

def build():
    """ වචන පිටු සැකසීම """
    cleanup_or_make_folder(path_public_words)
    create_index_page()
    clone_or_pull_repo(path_repo, URL_REPO)
    create_list_pages()
