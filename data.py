""" වෙබ් අඩවියට අවශ්‍ය විවිධ දත්ත """

from os import path

WORKING_DIR_PATH = path.dirname(path.realpath(__file__))
PATH_STATIC = path.join(WORKING_DIR_PATH, "static")

PATH_TEMPLATES = path.join(WORKING_DIR_PATH, "templates")
PATH_PUBLIC = path.join(WORKING_DIR_PATH, "public")
PATH_TMP = path.join(WORKING_DIR_PATH, ".tmp")

PATH_PUBLIC_STATIC = path.join(PATH_PUBLIC, "static")
PATH_PUBLIC_PRIVACY = path.join(PATH_PUBLIC, "privacy")

akuru = {
    1: 'අ',
    2: 'ආ',
    3: 'ඇ',
    4: 'ඈ',
    5: 'ඉ',
    6: 'ඊ',
    7: 'උ',
    8: 'ඌ',
    9: 'ඍ',
    10: 'ඎ',
    11: 'ඏ',
    12: 'ඐ',
    13: 'එ',
    14: 'ඒ',
    15: 'ඓ',
    16: 'ඔ',
    17: 'ඕ',
    18: 'ඖ',
    19: 'අං',
    20: 'අඃ',
    21: 'ක',
    22: 'ඛ',
    23: 'ග',
    24: 'ඝ',
    25: 'ඞ',
    26: 'ඟ',
    27: 'ච',
    28: 'ඡ',
    29: 'ජ',
    30: 'ඣ',
    31: 'ඤ',
    32: 'ඦ',
    33: 'ට',
    34: 'ඨ',
    35: 'ඩ',
    36: 'ඪ',
    37: 'ණ',
    38: 'ඬ',
    39: 'ත',
    40: 'ථ',
    41: 'ද',
    42: 'ධ',
    43: 'න',
    44: 'ඳ',
    45: 'ප',
    46: 'ඵ',
    47: 'බ',
    48: 'භ',
    49: 'ම',
    50: 'ඹ',
    51: 'ය',
    52: 'ර',
    53: 'ල',
    54: 'ව',
    55: 'ශ',
    56: 'ෂ',
    57: 'ස',
    58: 'හ',
    59: 'ළ',
    60: 'ෆ',
    61: 'ඥ'
}

swara = [akuru[x] for x in range(1, 21)]

swara_keti = [akuru[x] for x in [1, 3, 5, 7, 9, 11, 13, 16]]
swara_deerga = [akuru[x] for x in [2, 4, 6, 8, 10, 12, 14, 17]]

alpaprana = [akuru[x] for x in [21, 23, 27, 29, 33, 35, 39, 41, 45, 47]]
mahaprana = [akuru[x] for x in [22, 24, 28, 30, 34, 36, 40, 42, 46, 48]]

ghosha = [akuru[x] for x in [
    19, 23, 24, 25, 26, 29, 30,
    31, 32, 35, 36, 37, 38, 41,
    42, 43, 44, 47, 48, 49, 50,
    51, 52, 53, 54, 58, 59
]]
aghosha = [akuru[x] for x in [
    20, 21, 22, 27, 28,
    33, 34, 39, 40, 45,
    46, 55, 56, 57, 60
]]
